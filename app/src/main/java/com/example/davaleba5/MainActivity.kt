package com.example.davaleba5

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.example.davaleba5.room.Sport

class MainActivity : AppCompatActivity() {

    val submitButton = findViewById<Button>(R.id.submitButton)
    val ranDistanceText = findViewById<EditText>(R.id.ranDistanceText)
    val swamDistanceText = findViewById<EditText>(R.id.swamDistanceText)
    val caloriesTakenText = findViewById<EditText>(R.id.caloriesTakenText)
    val ranTextView = findViewById<TextView>(R.id.ranTextView)
    val swamTextView = findViewById<TextView>(R.id.swamTextView)
    val caloriesTextView = findViewById<TextView>(R.id.caloriesTextView)
    val len = App.instance.db.sportDao().getAllInfo().size



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        click()
        count()
    }

    private fun click(){
        submitButton.setOnClickListener {
            App.instance.db.sportDao().insert(
                Sport(distanceRan = ranDistanceText.toString().toDouble(),
                distanceSwam = swamDistanceText.toString().toDouble(),
                caloriesTaken = caloriesTakenText.toString().toInt())
            )
        }
    }

    private fun count(){

        val mileage = App.instance.db.sportDao().getAllInfo().sumOf { it.distanceRan }
        ranTextView.text = (mileage/len).toString()

        val swamDistance = App.instance.db.sportDao().getAllInfo().sumOf { it.distanceSwam }
        swamTextView.text = (swamDistance/len).toString()
    }

}