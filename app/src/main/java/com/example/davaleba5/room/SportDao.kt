package com.example.davaleba5.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.example.davaleba5.room.Sport

@Dao
interface SportDao {

    @Query("SELECT * FROM sports")
    fun getAllInfo(): List<Sport>

    @Insert
    fun insert(vararg sport: Sport)


}