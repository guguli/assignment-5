package com.example.davaleba5.room

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "SPORTS")
data class Sport (

    @PrimaryKey
    @ColumnInfo(name = "DISTANCE_RAN")
    val distanceRan: Double,

    @ColumnInfo(name = "DISTANCE_SWAM")
    val distanceSwam: Double,

    @ColumnInfo(name = "CALORIES_TAKEN")
    val caloriesTaken: Int


)