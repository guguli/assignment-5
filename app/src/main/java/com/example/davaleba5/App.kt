package com.example.davaleba5

import android.app.Application
import androidx.room.Room
import com.example.davaleba5.room.SportDatabase

class App: Application() {

    lateinit var db: SportDatabase

    companion object {
        lateinit var  instance: App
            private set
    }

    override fun onCreate() {
        super.onCreate()

        instance = this

        db = Room.databaseBuilder(
            applicationContext,
            SportDatabase::class.java,
            "SPORT_DATABASE"
        ).allowMainThreadQueries().build()
    }
}